void topupGame() {
	int inputGame;
	for (int i = 0; i <= 200; i++)
	{
		printf("\n");
	}
	puts("===========================================================\n");
	puts("       <> <> <>      == TOP UP GAME ==      <> <> <>       \n");
	puts("===========================================================\n");
	printf("[1] Garena Free Fire\n");
	printf("[2] PUBG MOBILE\n");
	printf("[3] Call of Duty: Mobile\n");
	printf("[0] Back to Main Menu\n");
	printf("\n");
	puts("===========================================================\n");
	do 
	{
		printf("\nInput a number: "); scanf("%d",&inputGame);
		getchar();
		switch (inputGame) 
		{
			case 1:
				gameEPEP();
				break;
			case 2:
				gamePUBGM();
				break;
			case 3:
				gameCODM();
				break;
			case 0:
				// mainMenu();
				break;
			default:
				printf("Input is not recognized.\n");
				break;
		}
	} while (inputGame < 0 || inputGame > 3);
}

void gameEPEP() {
	char playerID[100];
	int ctr, j = 0, temp = 1;
	for (int i = 0; i <= 200; i++)
	{
		printf("\n");
	}
	puts("===========================================================\n");
	puts("     <> <> <>     == GARENA : Free Fire ==     <> <> <>     \n");
	puts("===========================================================\n");
	printf("To find your PlayerID, click on your profile on the top-left corner\n");
	printf("of your screen. ");
	printf("Your Free Fire PlayerID will be displayed there.\n\n");
	puts("===========================================================\n");

	do
	{
		printf("Insert your PlayerID: "); scanf("%s",&playerID);
		int flag = 1, ctr = 0;
		for (int i = 0; playerID[i] != '\0'; i++)
		{
			ctr++;
		}
		if (ctr < 8 || ctr > 9)
		{
			puts("PlayerID is not recognized.\n");
			temp = 0;
			continue;
		}
		for (int j = 0; playerID[j] != '\0'; j++)
		{
			if (playerID[j] < 48 || playerID[j] > 57)
			{
				flag = 0;
				temp = 0;
				break;
			}
			temp = 1;
		}
		flag ? printf("PlayerID detected.\n") : printf("PlayerID format is illegal.\n");
	} while (temp == 0);
	printf("\n");
	epepTOPUP();
}

void gamePUBGM() {
	char playerID[100];
	int ctr, j = 0, temp = 1;
	
	for (int i = 0; i <= 200; i++)
	{
		printf("\n");
	}
	puts("===========================================================\n");
	puts("      <> <> <>       == PUBG MOBILE ==       <> <> <>      \n");
	puts("===========================================================\n");
	printf("To find your PlayerID, click on your avatar on the top-right corner\n");
	printf("of your screen. ");
	printf("Your PUBG MOBILE PlayerID will be displayed there.\n\n");
	puts("===========================================================\n");

	do
	{
		printf("Insert your PlayerID: "); scanf("%s",&playerID);
		int flag = 1, ctr = 0;
		for (int i = 0; playerID[i] != '\0'; i++)
		{
			ctr++;
		}
		if (ctr < 10 || ctr > 10)
		{
			puts("PlayerID is not recognized.\n");
			temp = 0;
			continue;
		}
		for (int j = 0; playerID[j] != '\0'; j++)
		{
			if (playerID[j] < 48 || playerID[j] > 57)
			{
				flag = 0;
				temp = 0;
				break;
			}
			temp = 1;
		}
		flag ? printf("PlayerID detected.\n") : printf("PlayerID format is illegal.\n");
	} while (temp == 0);
	printf("\n");
	pubgmTOPUP();
}

void gameCODM() {
	char openID[100];
	int ctr, j = 0, temp = 1;
	
	for (int i = 0; i <= 200; i++)
	{
		printf("\n");
	}
	puts("===========================================================\n");
	puts("    <> <> <>    == CALL OF DUTY:  MOBILE ==    <> <> <>    \n");
	puts("===========================================================\n");
	printf("To find your OpenID, press on \"Setting\" that is located on\n");
	printf("the top right corner of your screen then press on \"Legal and\n");
	printf("Privacy\". Your OpenID will be available there.\n\n");
	puts("===========================================================\n");

	do
	{
		printf("Insert your OpenID: "); scanf("%s",&openID);
		int flag = 1, ctr = 0;
		for (int i = 0; openID[i] != '\0'; i++)
		{
			ctr++;
		}
		if (ctr < 20 || ctr > 20)
		{
			puts("OpenID is not recognized.\n");
			temp = 0;
			continue;
		}
		for (int j = 0; openID[j] != '\0'; j++)
		{
			if (openID[j] < 48 || openID[j] > 57)
			{
				flag = 0;
				temp = 0;
				break;
			}
			temp = 1;
		}
		flag ? printf("OpenID detected.\n") : printf("OpenID format is illegal.\n");
	} while (temp == 0);
	printf("\n");
	codmTOPUP();
}

void epepTOPUP() {
	for (long long int i = 0; i <= 180000000; i++) {
		;
	}
	int inputUC;
	puts("===========================================================\n");
	puts("Please choose the amount of Free Fire Diamonds you wish to buy?\n");
	puts("===========================================================\n");
	printf("[1] 5 Diamonds\n"); // Rp 1rb
	printf("[2] 12 Diamonds\n"); // Rp 2rb
	printf("[3] 50 Diamonds\n"); // Rp 8rb
	printf("[4] 70 Diamonds\n"); // Rp 10rb
	printf("[5] 140 Diamonds UC\n"); // Rp 20rb
	printf("[9] Back to Top-Up Game Page\n");
	printf("[0] Back to Main Menu\n\n");
	puts("===========================================================");
	do 
	{
		printf("\nInput a number: "); scanf("%d",&inputUC);
		getchar();
		switch (inputUC) 
		{
			case 1:
				printf("It will cost Rp 1,000,-\n");
				// insertPIN();
				break;
			case 2:
				printf("It will cost Rp 2,000,-\n");
				// insertPIN();
				break;
			case 3:
				printf("It will cost Rp 8,000,-\n");
				// insertPIN();
				break;
			case 4:
				printf("It will cost Rp 10,000,-\n");
				// insertPIN();
				break;
			case 5:
				printf("It will cost Rp 20,000,-\n");
				// insertPIN();
				break;
			case 9:
				topupGame();
				break;
			case 0:
				// mainMenu();
				break;
			default:
				printf("Input is not recognized.\n");
				break;
		}
	} while ((inputUC < 0) || (5 < inputUC && inputUC < 9) || (inputUC > 9));
}

void pubgmTOPUP() {
	for (long long int i = 0; i <= 180000000; i++) {
		;
	}
	int inputUC;
	puts("===========================================================\n");
	puts("Please choose the amount of PUBG Mobile UC you wish to buy?\n");
	puts("===========================================================\n");
	printf("[1] 250+13 UC\n"); // Rp 50rb
	printf("[2] 500+25 UC\n"); // Rp 100rb
	printf("[3] 1250+125 UC\n"); // Rp 250rb
	printf("[4] 2500+375 UC\n"); // Rp 500rb
	printf("[5] 5000+1000 UC\n"); // Rp 1 000rb
	printf("[9] Back to Top-Up Game Page\n");
	printf("[0] Back to Main Menu\n\n");
	puts("===========================================================");
	do 
	{
		printf("\nInput a number: "); scanf("%d",&inputUC);
		getchar();
		switch (inputUC) 
		{
			case 1:
				printf("It will cost Rp 50,000,-\n");
				// insertPIN();
				break;
			case 2:
				printf("It will cost Rp 100,000,-\n");
				// insertPIN();
				break;
			case 3:
				printf("It will cost Rp 250,000,-\n");
				// insertPIN();
				break;
			case 4:
				printf("It will cost Rp 500,000,-\n");
				// insertPIN();
				break;
			case 5:
				printf("It will cost Rp 1,000,000,-\n");
				// insertPIN();
				break;
			case 9:
				topupGame();
				break;
			case 0:
				// mainMenu();
				break;
			default:
				printf("Input is not recognized.\n");
				break;
		}
	} while ((inputUC < 0) || (5 < inputUC && inputUC < 9) || (inputUC > 9));
}

void codmTOPUP() {
	for (long long int i = 0; i <= 180000000; i++) {
		;
	}
	int inputCP;
	puts("===========================================================\n");
	puts("Please choose the amount of COD Points you wish to buy?\n");
	puts("===========================================================\n");
	printf("[1] 317 CP\n"); // Rp 50rb
	printf("[2] 634 CP\n"); // Rp 100rb
	printf("[3] 1373 CP\n"); // Rp 200rb
	printf("[4] 3564 CP\n"); // Rp 500rb
	printf("[5] 7656 CP\n"); // Rp 1 000rb
	printf("[9] Back to Top-Up Game Page\n");
	printf("[0] Back to Main Menu\n\n");
	puts("===========================================================");
	do 
	{
		printf("\nInput a number: "); scanf("%d",&inputCP);
		getchar();
		switch (inputCP) 
		{
			case 1:
				printf("It will cost Rp 50,000,-\n");
				// insertPIN();
				break;
			case 2:
				printf("It will cost Rp 100,000,-\n");
				// insertPIN();
				break;
			case 3:
				printf("It will cost Rp 200,000,-\n");
				// insertPIN();
				break;
			case 4:
				printf("It will cost Rp 500,000,-\n");
				// insertPIN();
				break;
			case 5:
				printf("It will cost Rp 1,000,000,-\n");
				// insertPIN();
				break;
			case 9:
				topupGame();
				break;
			case 0:
				// mainMenu();
				break;
			default:
				printf("Input is not recognized.\n");
				break;
		}
	} while ((inputCP < 0) || (5 < inputCP && inputCP < 9) || (inputCP > 9));
}
